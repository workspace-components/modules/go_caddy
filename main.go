// Copyright 2015 Matthew Holt and The Caddy Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package main is the entry point of the Caddy application.
// Most of Caddy's functionality is provided through modules,
// which can be plugged in by adding their import below.
//
// There is no need to modify the Caddy source code to customize your
// builds. You can easily build a custom Caddy with these simple steps:
//
//  1. Copy this file (main.go) into a new folder
//  2. Edit the imports below to include the modules you want plugged in
//  3. Run `go mod init caddy`
//  4. Run `go install` or `go build` - you now have a custom binary!
//
// Or you can use xcaddy which does it all for you as a command:
// https://github.com/caddyserver/xcaddy
package main

import (
	caddycmd "github.com/caddyserver/caddy/v2/cmd"

	_ "github.com/RussellLuo/caddy-ext/dynamichandler"
	_ "github.com/RussellLuo/caddy-ext/flagr"
	_ "github.com/RussellLuo/caddy-ext/layer4"
	_ "github.com/RussellLuo/caddy-ext/ratelimit"
	_ "github.com/RussellLuo/caddy-ext/requestbodyvar"
	_ "github.com/abiosoft/caddy-json-parse"
	_ "github.com/caddy-dns/cloudflare"
	_ "github.com/enum-gg/caddy-discord@v1.2.0"
	_ "github.com/evalphobia/logrus_sentry@v0.8.2"
	_ "github.com/greenpau/caddy-security@v1.1.29"
	_ "github.com/lucaslorentz/caddy-docker-proxy/v2@v2.8.11"
	_ "github.com/pberkel/caddy-storage-redis@v1.2.0"
	_ "github.com/pteich/caddy-tlsconsul@v1.4.0"
	_ "github.com/tailscale/caddy-tailscale"
	_ "go.mongodb.org/mongo-driver/mongo@v1.15.0"
)

func main() {
	caddycmd.Main()
}
