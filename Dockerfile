FROM golang:1.22.2-bookworm AS builder
RUN sudo apt install -y debian-keyring debian-archive-keyring apt-transport-https \
 && curl -1sLf 'https://dl.cloudsmith.io/public/caddy/xcaddy/gpg.key' | sudo gpg --dearmor -o /usr/share/keyrings/caddy-xcaddy-archive-keyring.gpg \
&& curl -1sLf 'https://dl.cloudsmith.io/public/caddy/xcaddy/debian.deb.txt' | sudo tee /etc/apt/sources.list.d/caddy-xcaddy.list
RUN sudo apt update && sudo apt install xcaddy
RUN --mount=type=cache,target=/root/.cache/go-build \
  --mount=type=cache,target=/go/pkg \
  xcaddy build \
  --with github.com/tailscale/caddy-tailscale \
  --with github.com/pteich/caddy-tlsconsul@v1.4.0 \
  --with github.com/greenpau/caddy-security@v1.1.29 \
  --with github.com/enum-gg/caddy-discord@v1.2.0 \
  --with github.com/caddy-dns/cloudflare \
  --with github.com/RussellLuo/caddy-ext/dynamichandler \
  --with github.com/RussellLuo/caddy-ext/flagr \
  --with github.com/RussellLuo/caddy-ext/layer4 \
  --with github.com/RussellLuo/caddy-ext/ratelimit@v0.3.0 \
  --with github.com/RussellLuo/caddy-ext/requestbodyvar \
  --with github.com/abiosoft/caddy-json-parse \
  --with github.com/pberkel/caddy-storage-redis@v1.2.0 \
  --with github.com/lucaslorentz/caddy-docker-proxy/v2@v2.8.11 \
  --with github.com/evalphobia/logrus_sentry@v0.8.2 \
  --with go.mongodb.org/mongo-driver/mongo@v1.15.0
# --with github.com/caddy-dns/netlify@latest \
# --with github.com/dunglas/mercure/caddy@latest \
#   --with github.com/dunglas/vulcain/caddy@latest \

ARG CADDY_VERSION=2.7.6
FROM caddy:${CADDY_VERSION}-alpine
COPY --from=builder /usr/bin/caddy /usr/bin/caddy
